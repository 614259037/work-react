import React from 'react';

const Categories = ({ filterItems, showdata }) => {
  return (
    <div className="btn-container">
      {showdata.map((type,index) => {
        return (<button className="filter-btn" key={index} onClick={() => filterItems(type)}>
         {type}
        </button>);
      })}




    </div>
  );
};

export default Categories;
