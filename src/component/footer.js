import React, { Component } from 'react'
import '../css/footer.css'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Footer extends Component {
    render() {
        return (
            <div>
                <footer>
                    <p>Author: Hege Refsnes</p>
                    <p><a href="mailto:hege@example.com">hege@example.com</a></p>
                </footer>
            </div>
        )
    }
}
