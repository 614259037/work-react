import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/navh.css'
import NavItem from './NavItem';
export default class Navhead extends Component {
  constructor(props) {
    super(props);
    this.state = {
        'NavItemActive': ''
    }
  }
  render() {
    return (
      <div className=" posi-navh sticky-top">

        <nav className="navbar navbar-expand-lg navh-bg ">
          <a className="navbar-brand" href="1">Logo</a>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav nav-link btn">
              <li className="nav-item">
              <NavItem item="Home"  tolink="/"></NavItem>
              </li>
              <li className="nav-item">
              <NavItem item="Register"  tolink="/regis"></NavItem>
              </li>
              <li className="nav-item">
              <NavItem item="Login"  tolink="/login"></NavItem>
              </li>
              <li className="nav-item">
              <NavItem item="Profile"  tolink="/profile"></NavItem>
              </li>
              <li className="nav-item">
              <NavItem item="Cat"  tolink="/cat"></NavItem>
              </li>
            </ul>
          </div>
          <form className="form-inline " action="/action_page.php">
            <input className="form-control mr borde-s " type="text" placeholder="Search" /><i className="fas fa-search"></i>
           
          </form>
          <a href="#"><i className="fab fa-github"></i></a>
        </nav>


      </div>
    )
  }
}
