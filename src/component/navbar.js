import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Navbar extends Component {
    render() {
        return (
            <div className="container posi-nav">
      
            <nav className="navbar navbar-expand-lg navbg shadow  ">
              
              <div className="collapse navbar-collapse justify-content-center"  id="navbarNav">
               <ul className="navbar-nav">
                 <li className="nav-item">
                   <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                 </li>
                 <li className="nav-item">
                   <a className="nav-link" href="#">Features</a>
                 </li>
                 <li className="nav-item">
                   <a className="nav-link" href="#">Pricing</a>
                 </li>
                 <li className="nav-item">
                   <a className="nav-link disabled" href="#">Disabled</a>
                 </li>
                 <li className="nav-item active">
                   <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                 </li>
                 <li className="nav-item">
                   <a className="nav-link" href="#">Features</a>
                 </li>
                 <li className="nav-item">
                   <a className="nav-link" href="#">Pricing</a>
                 </li>
                 <li className="nav-item">
                   <a className="nav-link disabled" href="#">Disabled</a>
                 </li>
               </ul>
              </div>
           </nav>
     
     
         </div>
        )
    }
}
