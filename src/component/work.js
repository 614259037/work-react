import React, { Component } from 'react'
import Cardwork from '../content/cardwork';
import Review from '../content/review';
export default class Work extends Component {
    render() {
        return (
            <div className="m-1">
                <div className="row p-3">
                    <div className="col-3">
                        <div className="p-2">
                            <div className="row">
                                <div className="col">
                                    <div>
                                        <h1 >
                                            WORKING
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-9">
                        <div className="p-2">
                            <div className="row">
                                <div className="col-4">
                                    <Cardwork />
                                </div>
                                <div className="col-4">
                                    <Cardwork />
                                </div>
                                <div className="col-4">
                                    <Cardwork />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <hr></hr>
                <div className="row p-3">
                    <div className="col-3">
                        <div className="p-2">
                            <div className="row">
                                <div className="col">
                                    <div>
                                        <h1 >
                                            REVIEW
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-9">
                        <div className="p-2">
                            <div className="row">
                                <div className="col-12">
                                    <Review />
                                </div>
                                <div className="col-12">
                                    <Review />
                                </div>
                                
                               
                            
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
