import React, { Component } from 'react'
import Personal from './personal'
import Work from './work'

export default class Infomation extends Component {
    render() {
        return (
            <div className="mb-5">
                <ul className="nav nav-pills mt-3" id="pills-tab" role="tablist">
                    <li className="nav-item" role="presentation">
                        <a className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Personal</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="nav-link" id="pills-profile-tab" data-bs-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Work</a>
                    </li>
                </ul>
                <div className="tab-content card" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                        <Personal />

                    </div>
                    <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                        <Work />

                    </div>

                </div>
            </div>
        )
    }
}
