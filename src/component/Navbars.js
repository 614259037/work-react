import React, { Component } from 'react';
import NavItem from './NavItem';
export default class Navbars extends Component {
    constructor(props) {
        super(props);
        this.state = {
            'NavItemActive': ''
        }

    }
    render() {
        return (
            <nav>
                <ul>
                    <NavItem item="Home" tolink="/"></NavItem>
                    <NavItem item="About" tolink="/About"></NavItem>
                    <NavItem item="Login" tolink="/Login"></NavItem>
                </ul>
            </nav>
        )
    }
}
