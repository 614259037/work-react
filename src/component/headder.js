import React, { Component } from 'react'
// import logo from '../img/logo192.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactTypingEffect from 'react-typing-effect';
export default class Headder extends Component {
    render() {
        return (
            <div className="bg-head">
                <div className="posi-head">
                    <label className="size-head ">
                    <ReactTypingEffect className="color-font" text={'HEADDER'} speed="50" eraseSpeed="50" className="typingeffect"></ReactTypingEffect>
                </label>
                </div>
            </div>
        )
    }
}
