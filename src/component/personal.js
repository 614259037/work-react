import React, { Component } from 'react'

export class Personal extends Component {
    render() {
        return (
            <div className=" mt-2 ml-5 mr-5 mb-3">
                <h3>Name</h3>
                <label>Man</label>
                <h3>Address</h3>
                <label>34/2 M.2</label>
                <h3>E-mail</h3>
                <label>Mail@mail.com</label>
                <h3>Phone</h3>
                <label>0999999999</label>
                <h3>ID card </h3>
                <label>1234567890111</label>
                <h3>dd/mm/yy(birth)</h3>
                <label>01/01/2020</label><br></br>
                <button className="btn btn-primary btn-block" type="button" data-bs-toggle="modal" data-bs-target="#personal" data-bs-whatever="@getbootstrap">Edit</button>

                <div class="modal fade" id="personal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="mb-3">
                                        <label for="recipient-name" class="col-form-label">Name:</label>
                                        <input type="text" class="form-control" id="recipient-name" />
                                    </div>
                                    <div class="mb-3">
                                        <label for="message-text" class="col-form-label">Address:</label>
                                        <textarea class="form-control" id="message-text"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="recipient-name" class="col-form-label">E-mail:</label>
                                        <input type="text" class="form-control" id="recipient-name" />
                                    </div>
                                    <div class="mb-3">
                                        <label for="recipient-name" class="col-form-label">Phone:</label>
                                        <input type="number" class="form-control" id="recipient-name" maxLength="10"/>
                                    </div>
                                    <div class="mb-3">
                                        <label for="recipient-name" class="col-form-label">ID card:</label>
                                        <input type="number" class="form-control" id="recipient-name" maxLength="13"/>
                                    </div>
                                    <div class="mb-3">
                                        <label for="recipient-name" class="col-form-label">dd/mm/yy(birth):</label>
                                        <input type="date" class="form-control" id="recipient-name" />
                                    </div>
                                    
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Send message</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Personal;
