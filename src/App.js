import React from 'react';
import './css/App.css';
import './css/prof.css';
import Navbar from './component/navbar';
import Navhead from './component/navhead';
import Headder from './component/headder';
import Footer from './component/footer';
// import SignInSide from './content/login';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Regis from './content/regis';
import Login from './content/login';
import Profile from './content/profile';
import Cat from './content/cat';



import 'bootstrap/dist/css/bootstrap.min.css';
import Body from './content/body';
const Example = (props) => {
  return (
    <Router>
      <div>
        <Navhead />
        <Headder className="posi" />
        <Navbar />
        <Route exact path="/">
          <Body />
        </Route>
        <Route exact path="/regis">
          <Regis />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/profile">
          <Profile />
        </Route>
        <Route exact path="/cat">
          <Cat />
        </Route>
        {/* <Route exact path="/login">
          <SignInSide />
        </Route> */}
        <Footer />

      </div>
    </Router>
  );
}

export default Example;