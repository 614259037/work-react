import React from 'react';

const Personal = ({ items }) => {

  return (
    <div className="section-center">
      {items.map((menuItems) => {
        const { id, title, category, price, img, desc } = menuItems;
        return (
          <article key={id} className="menu-item">
            <img src={img} alt={title} className="photo" />
            <div className="item-info">
              <header>
                <h4>{title}</h4>
                <h4 className="price">${price}</h4>
              </header>
              <p className="item-text">{desc}</p>
              <button className="filter-btn">order now</button>

            </div>

          </article>
        )
      })}
    </div>
  );
};

export default Personal;
