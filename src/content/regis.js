import React, { Component } from 'react'
import axios from 'axios'
import api from '../Codeig_api'
import PasswordStrengthBar, { scoreWords } from 'react-password-strength-bar';

export default class Regis extends Component {
    constructor() {
        super();
        this.state = {
            password: "",
            scors: "",
        }
    }

    chkpass = () => {
        this.setState({ password: this.getpassword.value })
    }


    send = (e) => {
        e.preventDefault();

        const username = this.getusername.value;
        const password = this.getpassword.value;
        const chkpassword = this.getchkpassword.value;
        const fname = this.getfname.value;
        const lname = this.getlname.value;

        if (username === "") {
            alert('กรุณาใส่ชื่อผู้ใช้');
            document.regis.username.focus();
            return false;
        }
        if (fname === "") {
            alert('กรุณาใส่ชื่อจริง');
            document.regis.firstname.focus();
            return false;
        }
        if (lname === "") {
            alert('กรุณาใส่นามสกุล');
            document.regis.lastname.focus();
            return false;
        }

        if (password === "") {
            alert('กรุณาใส่รหัสผ่าน');
            document.regis.pass.focus();
            return false;
        }
        if (this.state.scors <= 2) {
            alert('รหัสผ่านไม่ปลอดภัย')
            document.regis.pass.focus();
            return false;
        }
        if (chkpassword === "") {
            alert('กรุณาใส่รหัสผ่าน');
            document.regis.chkpass.focus();
            return false;
        }

        if (password !== chkpassword) {
            alert('กรุณาใส่รหัสผ่านให้ตรงกัน');
            document.regis.pass.focus();
            return false;
        }
        else {
            axios.post(api('register'),
                JSON.stringify({

                    'username': username,
                    'password': password,
                    'fname': fname,
                    'lname': lname


                }))
                .then(res => {
                    if (res.data === 'success') {
                        alert('สมัครสมาชิกเรียบร้อย')
                        window.location.reload();
                    }
                    else {
                        alert('มีชื่อผู้ใช้งานนี้อยู่เเล้ว')

                    }
                })

        }

    }


    render() {
        return (
            <div className="container p-5 posi-reg">
                <div className="card-regis card shadow">
                    <h3>REGISTER</h3>
                    <form name="regis" onSubmit={this.send}>
                        <div className="mb-3">
                            <label for="exampleInputEmail1" className="form-label">UserName</label>
                            <input type="text" name="username" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" ref={(input) => this.getusername = input} />

                        </div>
                        <div className="mb-3">
                            <label for="exampleInputEmail1" className="form-label">FistName</label>
                            <input type="text" name="firstname" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" ref={(input) => this.getfname = input} />

                        </div>     <div className="mb-3">
                            <label for="exampleInputEmail1" className="form-label">LastName</label>
                            <input type="text" name="lastname" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" ref={(input) => this.getlname = input} />

                        </div>
                        <div className="mb-3">
                            <label for="exampleInputPassword1" className="form-label">Password (12 characters)</label>
                            <input type="password" name="pass" className="form-control" id="exampleInputPassword1" onChange={this.chkpass} ref={(input) => this.getpassword = input} />
                            <PasswordStrengthBar password={this.state.password} onChangeScore={score => {
                                this.setState({ scors: score })
                            }} />
                        </div>
                        <div className="mb-3">
                            <label for="exampleInputPassword1" className="form-label">confirm Password (12 characters)</label>
                            <input type="password" name="chkpass" className="form-control" id="exampleInputPassword1" ref={(input) => this.getchkpassword = input} />
                        </div>

                        <div className="mt-5 d-grid gap-2 d-md-flex justify-content-md-end">
                            <button name="sub" className="btn btn-primary me-md-2" type="submit">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

